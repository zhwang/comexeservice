// ComExeService.cpp : Implementation of WinMain


#include "stdafx.h"
#include "resource.h"
#include "ComExeService_i.h"


using namespace ATL;


class CComExeServiceModule : public ATL::CAtlExeModuleT< CComExeServiceModule >
{
public :
	DECLARE_LIBID(LIBID_ComExeServiceLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_COMEXESERVICE, "{3d7704d1-8a08-424b-b7e3-627caccaf99a}")
};

CComExeServiceModule _AtlModule;



//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/,
								LPTSTR /*lpCmdLine*/, int nShowCmd)
{
	return _AtlModule.WinMain(nShowCmd);
}

