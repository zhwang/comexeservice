// Worker.cpp : Implementation of CWorker

#include "stdafx.h"
#include "Worker.h"
#include <future>

// CWorker
HRESULT func(CWorker* pWorker,LONG val)
{
	while(true)
	{
		Sleep(1000);
		if (pWorker)
		{
			pWorker->Fire_DoWorkerEvents(val++);
		}
	}
	return S_OK;
}

STDMETHODIMP CWorker::Initialize(LONG p)
{
	std::future<HRESULT> result_future = std::async(func, this, p);
	
	//Fire_DoWorkerEvents(123 + p);
	
	return S_OK;
}
