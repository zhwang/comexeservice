#pragma once
class CSinkDll : public _IDllWorkerEvents
{
public:
	CSinkDll()
	{
		m_dwRefCount = 0;

	}
	virtual ~CSinkDll();

private:
	DWORD       m_dwRefCount;
public:

	HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, void **ppvObject)
	{
		if (iid == IID__IDllWorkerEvents)
		{
			m_dwRefCount++;
			*ppvObject = (void *)this;
			return S_OK;
		}

		if (iid == IID_IUnknown)
		{
			m_dwRefCount++;
			*ppvObject = (void *)this;
			return S_OK;
		}

		return E_NOINTERFACE;
	}
	ULONG STDMETHODCALLTYPE AddRef()
	{
		m_dwRefCount++;
		return m_dwRefCount;
	}

	ULONG STDMETHODCALLTYPE Release()
	{
		ULONG l;

		l = m_dwRefCount--;

		if (0 == m_dwRefCount)
		{
			delete this;
		}

		return l;
	}

	HRESULT STDMETHODCALLTYPE DoWorkerEvents(LONG parm)
	{
		return S_OK;
	}
};

