// DllWorker.h : Declaration of the CDllWorker

#pragma once
#include "resource.h"       // main symbols



#include "ComDllService_i.h"
#include "_IDllWorkerEvents_CP.h"



#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;


// CDllWorker

class ATL_NO_VTABLE CDllWorker :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CDllWorker, &CLSID_DllWorker>,
	public IConnectionPointContainerImpl<CDllWorker>,
	public CProxy_IDllWorkerEvents<CDllWorker>,
	public IDispatchImpl<IDllWorker, &IID_IDllWorker, &LIBID_ComDllServiceLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CDllWorker()
	{
	}

DECLARE_REGISTRY_RESOURCEID(106)


BEGIN_COM_MAP(CDllWorker)
	COM_INTERFACE_ENTRY(IDllWorker)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(CDllWorker)
	CONNECTION_POINT_ENTRY(__uuidof(_IDllWorkerEvents))
END_CONNECTION_POINT_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

	STDMETHOD(Initialize)(LONG p);


};

OBJECT_ENTRY_AUTO(__uuidof(DllWorker), CDllWorker)
