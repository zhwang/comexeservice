// dllmain.h : Declaration of module class.

class CComDllServiceModule : public ATL::CAtlDllModuleT< CComDllServiceModule >
{
public :
	DECLARE_LIBID(LIBID_ComDllServiceLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_COMDLLSERVICE, "{a8718e49-4e78-4442-b368-c4633dcddc39}")
};

extern class CComDllServiceModule _AtlModule;
