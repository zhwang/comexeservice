// ATLProject3.cpp : Implementation of WinMain


#include "stdafx.h"
#include "resource.h"
#include "ATLProject3_i.h"
#include "xdlldata.h"


using namespace ATL;


class CATLProject3Module : public ATL::CAtlExeModuleT< CATLProject3Module >
{
public :
	DECLARE_LIBID(LIBID_ATLProject3Lib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_ATLPROJECT3, "{f159b0dd-b745-4bcc-bdf3-50bfae446b78}")
};

CATLProject3Module _AtlModule;



//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/,
								LPTSTR /*lpCmdLine*/, int nShowCmd)
{
	return _AtlModule.WinMain(nShowCmd);
}

