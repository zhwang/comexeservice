// DispWorker.cpp : Implementation of CDispWorker

#include "stdafx.h"
#include "DispWorker.h"


// CDispWorker

#include "stdafx.h"
#include <future>

// CWorker
HRESULT func(CDispWorker* pWorker, LONG val)
{
	while (true)
	{
		Sleep(1000);
		if (pWorker)
		{
			pWorker->Fire_DoWorkerEvents(val++);
		}
	}
	return S_OK;
}

STDMETHODIMP CDispWorker::Initialize(LONG p)
{
	//std::future<HRESULT> result_future = std::async(func, this, p);

	Fire_DoWorkerEvents(123 + p);

	return S_OK;
}
